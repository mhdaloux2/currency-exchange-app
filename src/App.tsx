import React from 'react';
import logo from './logo.svg';
import './App.scss';
import CurrencyConverterForm from "./components/currency-converter-form/CurrencyConverterForm";
import HistoricalRatesGraph from "./components/historical-rates-graph/HistoricalRatesGraph";

export default class App extends React.Component <{}> {

    state = {
        baseCurrency: undefined,
        targetCurrency: undefined
    }

    handleCurrencySelection = (selected: any) => {
        this.setState({
            baseCurrency: selected.baseCurrency,
            targetCurrency: selected.targetCurrency
        });
    }

    render(): React.ReactNode {
        return (
            <div className="container container-lg">
                <header className="App-header mb-5">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <div className="App-title">Currency Converter</div>
                </header>
                <div className="mb-5">
                    <CurrencyConverterForm onSelectCurrency={this.handleCurrencySelection} />
                </div>
                <div className="mb-5">
                    <HistoricalRatesGraph baseCurrency={this.state.baseCurrency} targetCurrency={this.state.targetCurrency} />
                </div>
            </div>
        );
    }
}
