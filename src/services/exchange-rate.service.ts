import axios from "axios";
import {HistoricalRates} from "../components/historical-rates-graph/HistoricalRatesGraph.model";
import {API_KEY, BASE_URL} from "../app.config";

export interface CurrencyExchangeRate {
    fromCurrency: string,
    toCurrency: string,
    exchangeRate: number,
}

export default class ExchangeRateService {
    private previousCurrencyExchangeRateState: CurrencyExchangeRate = {
        fromCurrency: "",
        toCurrency: "",
        exchangeRate: 0,
    };

    private previousHistoricalRatesState: HistoricalRates = {
        fromCurrency: undefined,
        toCurrency: undefined,
        historicalRates: [],
    }

    getCurrencyExchangeRate(fromCurrency: string, toCurrency: string): Promise<number> {
        return new Promise((resolve, reject) => {
            // We don't need to make another call if the selected currencies are previously selected
            if (fromCurrency === this.previousCurrencyExchangeRateState.fromCurrency && toCurrency === this.previousCurrencyExchangeRateState.toCurrency) {
                resolve(this.previousCurrencyExchangeRateState.exchangeRate);
            } else {
                axios
                    .get(`${BASE_URL}/query?function=CURRENCY_EXCHANGE_RATE&from_currency=${fromCurrency}&to_currency=${toCurrency}&apikey=${API_KEY}`)
                    .then((response: any) => {
                        this.previousCurrencyExchangeRateState = {
                            fromCurrency: fromCurrency,
                            toCurrency: toCurrency,
                            exchangeRate: response.data["Realtime Currency Exchange Rate"]["5. Exchange Rate"],
                        };
                        resolve(response.data["Realtime Currency Exchange Rate"]["5. Exchange Rate"]);
                    }, (err) => {
                        reject(err);
                    })
            }
        });
    }

    getHistoricalRates(fromCurrency: string | undefined, toCurrency: string | undefined): Promise<any> {
        return new Promise((resolve, reject) => {
            // We don't need to make another call if the selected currencies are previously selected
            if (fromCurrency === undefined || toCurrency === undefined || (fromCurrency === this.previousHistoricalRatesState.fromCurrency && toCurrency === this.previousHistoricalRatesState.toCurrency)) {
                resolve(this.previousHistoricalRatesState.historicalRates);
            } else {
                axios
                    .get(`${BASE_URL}/query?function=FX_DAILY&from_symbol=${fromCurrency}&to_symbol=${toCurrency}&apikey=${API_KEY}`)
                    .then((response: any) => {
                        if (response.data['Time Series FX (Daily)']) {
                            this.previousHistoricalRatesState = {
                                fromCurrency: fromCurrency,
                                toCurrency: toCurrency,
                                historicalRates: Object.keys(response.data['Time Series FX (Daily)']).slice(0, 30).reverse().map(item => ({
                                    date: item,
                                    open: parseFloat(response.data['Time Series FX (Daily)'][item]['1. open']),
                                    high: parseFloat(response.data['Time Series FX (Daily)'][item]['2. high']),
                                    low: parseFloat(response.data['Time Series FX (Daily)'][item]['3. low']),
                                    close: parseFloat(response.data['Time Series FX (Daily)'][item]['4. close']),
                                })),
                            };
                            resolve(this.previousHistoricalRatesState.historicalRates);
                        } else {
                            console.error(response.data["Note"]);
                        }
                    }, (err) => {
                        reject(err);
                    })
            }
        });
    }
}
