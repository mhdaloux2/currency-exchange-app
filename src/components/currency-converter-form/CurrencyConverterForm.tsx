import React, {Component} from 'react';
import ExchangeRateService from "../../services/exchange-rate.service";
import {CurrencyConverterFormProps, CurrencyExchangeRateState, CurrencyOption} from "./CurrencyConverterForm.model";
import {options} from "./CurrencyOptions.data";

export default class CurrencyConverterForm extends Component<CurrencyConverterFormProps, CurrencyExchangeRateState> {
    private exchangeRateService = new ExchangeRateService();
    private currencyList: CurrencyOption[] = options;

    state: CurrencyExchangeRateState = {
        baseAmount: 1,
        targetAmount: undefined,
        baseCurrency: "USD",
        targetCurrency: "EUR",
    }

    constructor(props: CurrencyConverterFormProps) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSwitchCurrency = this.handleSwitchCurrency.bind(this);
    }

    handleSubmit(event: any) {
        event.preventDefault();
        this.exchangeRateService.getCurrencyExchangeRate(this.state.baseCurrency, this.state.targetCurrency)
            .then((exchangeRate) => {
                if (this.state.baseAmount) {
                    this.setState({targetAmount: this.calculateCurrency(this.state.baseAmount, exchangeRate)});
                }
                this.props.onSelectCurrency({
                    baseCurrency: this.state.baseCurrency,
                    targetCurrency: this.state.targetCurrency
                });
            });
    }

    calculateCurrency(amount: number, exchangeRate: number) {
        return amount * exchangeRate;
    }

    handleFieldChange(event: any) {
        let refreshState: any = {};
        refreshState[event.target.id] = event.target.value;
        refreshState.targetAmount = undefined;
        this.setState(refreshState);
    }

    handleSwitchCurrency() {
        let aux = this.state.baseCurrency;
        this.setState({
            baseCurrency: this.state.targetCurrency,
            targetCurrency: aux,
            targetAmount: undefined
        });
    }

    render(): React.ReactNode {
        let resultElement;
        const displayCurrencyList = (filtered: string) => {
            return <>{
                this.currencyList.map((currency: CurrencyOption, index: number) =>
                    filtered !== currency.value ?
                        <option key={index} value={currency.value}>{currency.value} - {currency.name}</option> : ""
                )}</>;
        }
        const displayCurrencyName = (value: string): string => {
            const found = this.currencyList.find(item => item.value === value);
            return found ? found.name : "";
        }
        const displayCurrencySymbol = (targetCurrency: string) => {
            const found = this.currencyList.find(item => item.value === targetCurrency);
            return found ? (found.symbol || found.value) : "";
        }
        if (this.state.targetAmount) {
            resultElement = <div className="Result">
                <p>{this.state.baseAmount} {displayCurrencyName(this.state.baseCurrency)} =</p>
                <h3>{this.state.targetAmount} {displayCurrencyName(this.state.targetCurrency)}</h3>
            </div>;
        } else {
            resultElement = null;
        }

        return (
            <div className="CurrencyConverterForm">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title mb-4">Conversion</h5>
                        <form className="row needs-validation" noValidate onSubmit={this.handleSubmit}>
                            <div className="col-sm-3 col-xs-12">
                                <div className="mb-3">
                                    <label htmlFor="baseAmount" className="form-label">Amount</label>
                                    <div className="input-group has-validation">
                                        <span className="input-group-text"
                                              id="inputAmountCurrency">{displayCurrencySymbol(this.state.baseCurrency)}</span>
                                        <input type="text" value={this.state.baseAmount}
                                               onChange={this.handleFieldChange}
                                               className="form-control" aria-describedby="inputAmountCurrency"
                                               id="baseAmount" required/>
                                        <div className="invalid-tooltip">Please enter a valid amount</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4 col-xs-5">
                                <label htmlFor="baseCurrency" className="form-label">From</label>
                                <select value={this.state.baseCurrency} onChange={this.handleFieldChange} id="baseCurrency"
                                        className="form-control">
                                    {displayCurrencyList(this.state.targetCurrency)}
                                </select>
                            </div>
                            <div className="col-sm-1 col-xs-2 text-center">
                                <label htmlFor="" className="form-label">&nbsp;</label>
                                <button type="button" onClick={this.handleSwitchCurrency}
                                        className="btn d-block mx-auto btn-outline-secondary">&#8596;
                                </button>
                            </div>
                            <div className="col-sm-4 col-xs-5">
                                <label htmlFor="targetCurrency" className="form-label">To</label>
                                <select value={this.state.targetCurrency} id="targetCurrency" onChange={this.handleFieldChange}
                                        className="form-control">
                                    {displayCurrencyList(this.state.baseCurrency)}
                                </select>
                            </div>
                            <div className="col-xs-12 text-end">
                                <button className="btn btn-primary" type="submit">Convert</button>
                            </div>
                        </form>
                        {resultElement}
                    </div>
                </div>
            </div>
        );
    }
}
