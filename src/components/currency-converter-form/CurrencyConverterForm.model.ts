export interface CurrencyConverterFormProps {
    onSelectCurrency: Function
}

export interface CurrencyExchangeRateState {
    baseAmount: number | undefined,
    baseCurrency: string,
    targetAmount: number | undefined;
    targetCurrency: string
}

export interface CurrencyOption {
    name: string,
    value: string,
    symbol?: string
}
