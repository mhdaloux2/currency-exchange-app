export interface HistoricalRatesProps {
    baseCurrency: string | undefined,
    targetCurrency: string | undefined
}

export interface HistoricalRatesState {
    historicalRates: HistoricalRate[],
    loading: boolean
}

export interface HistoricalRate {
    date: string,
    open: number,
    high: number,
    low: number,
    close: number
}

export interface HistoricalRates {
    fromCurrency: string | undefined,
    toCurrency: string | undefined,
    historicalRates: HistoricalRate[]
}
