import {Component} from "react";
import {HistoricalRate, HistoricalRatesProps, HistoricalRatesState} from "./HistoricalRatesGraph.model";
import ExchangeRateService from "../../services/exchange-rate.service";
import {Line} from 'react-chartjs-2';
import {ChartData} from "chart.js";

export default class HistoricalRatesGraph extends Component<HistoricalRatesProps, HistoricalRatesState> {
    private exchangeRateService = new ExchangeRateService();
    state: HistoricalRatesState = {
        historicalRates: [],
        loading: false
    }

    constructor(props: HistoricalRatesProps) {
        super(props);
        this.handleHistoricalRatesChange = this.handleHistoricalRatesChange.bind(this);
    }


    handleHistoricalRatesChange() {
        this.setState({loading: true});
        this.exchangeRateService.getHistoricalRates(this.props.baseCurrency, this.props.targetCurrency)
            .then((response: any) => {
                this.setState({
                    historicalRates: response,
                    loading: false
                });
            });
    }

    componentDidUpdate(prevProps: HistoricalRatesProps) {
        if (prevProps.baseCurrency !== this.props.baseCurrency || prevProps.baseCurrency !== this.props.baseCurrency) {
            this.handleHistoricalRatesChange();
        }
    }

    render(): React.ReactNode {
        function displayChartData(historicalRates: HistoricalRate[]): ChartData {
            return {
                labels: historicalRates.map(item => item.date),
                datasets: [
                    {
                        label: 'Open rate',
                        data: historicalRates.map(item => item.open),
                        backgroundColor: '#0a58ca',
                        borderColor: '#0a58ca'
                    },
                    {
                        label: 'High rate',
                        data: historicalRates.map(item => item.high),
                        backgroundColor: '#dc3545',
                        borderColor: '#dc3545'
                    },
                    {
                        label: 'Low rate',
                        data: historicalRates.map(item => item.low),
                        backgroundColor: '#51585e',
                        borderColor: '#51585e'
                    },
                    {
                        label: 'Close rate',
                        data: historicalRates.map(item => item.close),
                        backgroundColor: '#25cff2',
                        borderColor: '#25cff2'
                    }
                ]
            } as ChartData
        }

        if (this.state.historicalRates && this.state.historicalRates.length) {
            return <div className="card">
                <div className="card-body">
                    <h5 className="card-title mb-4">{this.props.baseCurrency} to {this.props.targetCurrency} Chart</h5>
                    <Line data={displayChartData(this.state.historicalRates)}/>
                </div>
            </div>;
        } else if (this.state.loading) {
            return <div className="card">
                <div className="card-body">
                    <h5 className="card-title mb-4">{this.props.baseCurrency} to {this.props.targetCurrency} Chart</h5>
                    Loading...
                </div>
            </div>
        } else return <></>;
    }
}
